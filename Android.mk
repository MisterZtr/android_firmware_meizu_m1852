LOCAL_PATH := $(call my-dir)

ifeq ($(TARGET_DEVICE),m1852)
#----------------------------------------------------------------------
# Firmware images
#----------------------------------------------------------------------
$(warning NOTICE! NOTICE! NOTICE! NOTICE! NOTICE! NOTICE! NOTICE!)
$(warning Including firmware images to radio files for m1852!)
$(warning NOTICE! NOTICE! NOTICE! NOTICE! NOTICE! NOTICE! NOTICE!)

images_dir := $(LOCAL_PATH)/firmware
FIRMWARE_FILES := $(shell cd $(images_dir) ; ls)
$(foreach f, $(FIRMWARE_FILES), \
	$(call add-radio-file,firmware/$(f)))

endif
